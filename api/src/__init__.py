from flask_cors import CORS
from flask import Flask, jsonify
from logging import basicConfig
from datetime import datetime
import src.logging
import os

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

import src.settings

import src.endpoints

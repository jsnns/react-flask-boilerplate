from logging import basicConfig
from datetime import datetime
import os

date_string = datetime.now().strftime('%Y/%m/%d/%H:%M:%S')
log_name = f"/app/logs/{date_string}.log"

basedir = os.path.dirname(log_name)
if not os.path.exists(basedir):
  os.makedirs(basedir)

if not os.path.exists(log_name):
  print(f"new log file at {log_name}")
  open(log_name, "a").close()

basicConfig(format='[%(asctime)s] %(levelname)s in %(module)s: %(message)s', filename=log_name, level="INFO")

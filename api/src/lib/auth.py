import os
from flask import request, Response
from src import app
from functools import wraps
import logging
import json

def check_auth(username, password):
    with open('src/lib/creds/creds.json') as f:
        creds = json.load(f)
        """This function is called to check if a username /
        password combination is valid.
        """
        if username in creds:
            valid_credentials = creds[username] == password
        if not valid_credentials:
            logging.info(f"denying auth to {username}, invalid username or password")
        else:
            logging.info(f"successfully authenticating {username}")
        return valid_credentials

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
      auth = request.authorization
      if not auth or not check_auth(auth.username, auth.password):
          return authenticate()
      return f(*args, **kwargs)
  return decorated

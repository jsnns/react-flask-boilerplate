from flask import jsonify
from src import app
from src.lib.auth import requires_auth

@app.route("/auth")
@requires_auth
def auth():
  return jsonify({"success": True})

import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Route } from "react-router";

/**
 * Root app component
 * @memberof module:App
 */
class App extends Component {
  render() {
    const RootComponent = () => <div>ReMF Boilerplate</div>;

    return (
      <BrowserRouter>
        <Route path="/" component={RootComponent} />
      </BrowserRouter>
    );
  }
}

export default App;

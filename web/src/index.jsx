/**
 * @author Jacob Sansbury
 * @module App
 */

import React from "react";
import ReactDOM from "react-dom";
import App from "./AppRouter";

window.ZoomChartsLicense = fetch(process.env.REACT_APP_ZOOM_LICENSE);
window.ZoomChartsLicenseKey = fetch(process.env.REACT_APP_ZOOM_API_KEY);

ReactDOM.render(<App />, document.getElementById("root"));
